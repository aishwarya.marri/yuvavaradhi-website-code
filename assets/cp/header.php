<!doctype html>
<html lang="en">
  <head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <nav class="navbar" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html">Yuva Varadhi</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
					<ul class="nav navbar-nav navbar-right">						
						<ul class="nav navbar-nav navbar-right">						
						<li class="active">
							<a href="index.php"><span aria-hidden="true" class="icon_house"></span><br>Home</a>
						</li>
						<li >
							<a href="ouractivities.html"><span aria-hidden="true" class="icon_mail"></span><br>Our Activities</a>
						</li>
						<li>
							<a href="portfolio.html"><span aria-hidden="true" class="icon_camera"></span><br>Gallery</a>
						</li>
						
						
						<li>
							<a href="about.html"><span aria-hidden="true" class="icon_info"></span><br>About Us</a>
						</li>
						<li >
							<a href="contact.html"><span aria-hidden="true" class="icon_mail"></span><br>Contact Us</a>
						</li>
						<li >
							<a href="cp/index.php"><span aria-hidden="true" class="icon_profile"></span><br>Volunteer Login</a>
						</li>
					         <li >
							<a href="cp/admin-login.php"><span aria-hidden="true" class="icon_profile"></span><br>Admin Login</a>
						</li>

					
					
					</ul>
				</div>
			</div>
		</nav>
