<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Yuvavaradhi Complaint Form</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&sensor=false">
    </script>
    
    <script type="text/javascript">
var markers = [

];
    </script>
    <script type="text/javascript">
        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 15,
                //mapTypeId: google.maps.MapTypeId.ROADMAP
                mapTypeId: google.maps.MapTypeId.HYBRID
            };
            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
            for (i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title
                });
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.description);
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
        }
    </script>
</head>
<body>
    <form method="post" action="./" onsubmit="javascript:return WebForm_OnSubmit();" id="form1" class="register">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE5NjgzNDc5MzkPZBYCAgMPZBYQAjUPEGRkFgFmZAI5DxBkZBYBZmQCPQ8QZGQWAWZkAkMPEGRkFgFmZAJHD2QWAgIDDw8WAh4EVGV4dGVkZAJJD2QWAgIDDxBkZBYBZmQCSw9kFgICAw8QZGQWAWZkAk0PZBYCAgMPEGRkFgFmZGTL3owOP28IEu6Hfj5aoshkj/ugYkVCIt7fmT1MwCNGGQ==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/gmitra/WebResource.axd?d=RcUWI9eLSy3ilWfVqltZtCvHNJCHNIvdyXh5Kzc7AZ2-bot1FZAkrasssrHLlXidfYeuY1cyNX-6chlEN4YeXE3Gg6PqyyIwY9s3gDkiAG41&amp;t=636065383469690049" type="text/javascript"></script>


<script src="/gmitra/WebResource.axd?d=9aBkT_0T_-qeDLavQohpGi3yNdU5FMvBK2t2mwA0GDf33AYMf1iINFW89Z4lMqGPXhAN4nedbR6Z7dRryLm6PI_l-wqcIR4ax0xb5OjMmz81&amp;t=636065383469690049" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
if (typeof(ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) return false;
return true;
}
//]]>
</script>
<style>
* /*Set's border, padding and margin to 0 for all values*/ /*Set's border, padding and margin to 0 for all values*/
{
    padding: 0;
    margin: 0;
    border: 0;
}
body, html
{
    color: #373C40;
    font-family: Verdana,Arial, Helvetica, sans-serif;
    height: 100%;  background-color: #f0f0f0;
    margin: 10px;    
    /* background: #DCDDDF url(https://cssdeck.com/uploads/media/items/7/7AF2Qzt.png);*/
  /*  background: url(../img/pattern1.png) repeat left top;*/
   /*  background: rgba(6, 41, 61, 1);*/
}
body
{
    font-size: 70%;
}
p
{
    padding: 7px 0 7px 0;
    font-weight: 500;
    font-size: 10pt;
}
a
{
    color: #656565;
    text-decoration: none;
}
a:hover
{
    color: #abda0f;
    text-decoration: none;
}
h1
{
    font-weight: 200;
    color: #2a6496;
    font-size: 16pt;
    background: transparent url(../img/h1.png) no-repeat center left;
    padding-left: 33px;
    margin: 7px 5px 8px 8px;
}
h4
{
    padding: 1px;
    color: #ACACAC;
    font-size: 9pt;
    font-weight: 100;
    text-transform: uppercase;
}
form.register
{
    width: 800px;
    margin: 20px auto 0px auto;
    height: 740px;
    background-color: #fff;
    padding: 5px;
    -moz-border-radius: 20px;
    -webkit-border-radius: 20px;
}
form p
{
    font-size: 8pt;
    clear: both;
    margin: 0;
    color: gray;
    padding: 4px;
}
form.register fieldset.row1
{
    width: 770px;
    padding: 5px;
    float: left;
    border-top: 1px solid #F5F5F5;
    margin-bottom: 15px;
}
form.register fieldset.row1 label
{
    width: 140px;
    float: left;
    text-align: right;
    margin-right: 6px;
    margin-top: 2px;
}
form.register fieldset.row2
{
    border-top: 1px solid #F1F1F1;
    border-right: 1px solid #F1F1F1;
    height: 220px;
    padding: 10px;
    float: left;
}
form.register fieldset.row3
{
    border-top: 1px solid #F1F1F1;
    padding: 5px;
    float: left;
    margin-bottom: 15px;
    width: 400px;
}
form.register fieldset.row4
{
    border-top: 1px solid #F1F1F1;
    border-right: 1px solid #F1F1F1;
    padding: 5px;
    float: left;
    clear: both;
    width: 810px;
}
form.register .infobox
{
    float: right;
    margin-top: 20px;
    border: 1px solid #F1F1F1;
    padding: 5px;
    width: 380px;
    height: 98px;
    font-size: 9px;
    background: #FDFEFA url(../img/bg_infobox.gif) repeat-x top left;
}
form.register legend
{
    color: #2a6496;
    padding: 2px;
    margin-left: 14px;
    font-weight: bold;
    font-size: 14px;
    font-weight: 100;
}
form.register label
{
    color: #444;
    width: 98px;
    float: left;
    text-align: right;
    margin-right: 6px;
    margin-top: 2px;
}
form.register label.optional
{
    float: left;
    text-align: right;
    margin-right: 6px;
    margin-top: 2px;
    color: #A3A3A3;
}
form.register label.obinfo
{
    float: right;
    padding: 3px;
    font-style: italic;
}
form.register input
{
    width: 140px;
    color: #505050;
    float: left;
    margin-right: 5px;
}
form.register input.long
{
    width: 240px;
    color: #505050;
    border: 1px;
}
form.register input.short
{
    width: 40px;
    color: #505050;
}
form.register input[type=radio]
{
    float: left; /*width: 15px;*/
}
form.register label.gender
{
    margin-top: -1px;
    margin-bottom: 2px;
    width: 34px;
    float: left;
    text-align: left;
    line-height: 19px;
}
form.register input[type=text]
{
    border: 1px solid #E1E1E1;
    height: 18px;
}
form.register input[type=password]
{
    border: 1px solid #E1E1E1;
    height: 18px;
}
.button
{
    background: #2a6496 url(../img/overlay.png) repeat-x;
    padding: 8px 10px 8px;
    color: #fff;
    text-decoration: none;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -moz-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
    -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
    text-shadow: 0 -1px 1px rgba(0,0,0,0.25);
    cursor: pointer;
    float: left;
    font-size: 18px;
    margin: 10px;
}
form.register input[type=text].year
{
    border: 1px solid #E1E1E1;
    height: 18px;
    width: 30px;
}
form.register input[type=checkbox]
{
    width: 14px;
    margin-top: 4px;
}
form.register select
{
    border: 1px solid #E1E1E1;
    width: 130px;
    float: left;
    margin-bottom: 3px;
    color: #505050;
    margin-right: 5px;
}
form.register select.date
{
    width: 40px;
}
input:focus, select:focus
{
    background-color: #efffe0;
}
p.info
{
    font-size: 7pt;
    color: gray;
}
p.agreement
{
    margin-left: 15px;
}
p.agreement label
{
    width: 390px;
    text-align: left;
    margin-top: 3px;
}
.font
{
    font-size: small;
}

</style>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AE18A1ED" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdACxszoVZqBsSdEZPg6FdPQfJn37wiCR0CcKeZqrz2fGzH5m/Xm56dQ/CEIHfHnMizmr/ptw17B1IMi9gXxNS0uuccRX45Z5LZ++IY3+Snm7zLEfmUjOiSav8SoIrYPiPls5Ju9M/Y8hzIbUjCkEDR2FLvEnuMPtZk02azN0EGeNA0BlGOKrIRk/fiS9cYRIZaSwRDOb5zSUllVlps3AdGEDyM63a5fxkUfKMjBlsRBnQrXDUyc5rXf16Dnxx7IGuvY0i81pdo3d0rKIF1noSWSNmMhJm+3uQ/EhPEpO0bI+28CVA8gx9Wy6XXmKURv8iDDKllxjo7zCabJwAE17KsVmEtO9a8rdM/o9iagI6Ieh0CCxfXVjPJZ5ghX97BUTPKrVpRPebwc8rrth8Qkhx1jH9CGgFolrRBuyG75n8+/uqTvDxM+3Y4Cr5rO3uMfa0JtEQesLN1R7s7wXWSexuy8q2AsE0auPCzFysp93TlebarUAbwO4Ry5+P15spNoFTsM2eHDlbUaNda11s2uRs6wEFX+OZEQMpBgx14/cnBlIhd9i1zCVW6bG8NHC1cQxBEHbXxt0prbVa5KVwEtYv9vTjtObXcV6qUzjXp0co8ZEBsdBYuVcC1g1mxzY7NAxEy9WOCQBg97ZQXsEdgKPXT5a0LtH8z6izyEQJzYpNcI2N2KwYORY3ck0W9U9KkxPnKGTeeeMdXx/86bIAa08OdxXNag0dncDDhQQ6UaH+e/K1msULZGp7GDpL6ZlE6bSkTIhW3ZVqaKvaobuPqMAWHyHQ0xm+S68VVjoKgmch5mPf1ofYdZfBN/4Xhowb97MrtVF/eokYk0KYvRWDTaDbbn6jhpF2Blh4NxZxGTKxnNY8oaMQi2j+YKxL5GA5YDnP5WqOu1yZfFa90XCv4oIYbUeVPOaW1pQztoQA36D1w/+bXaPMYgoFfIgna+KAVvJzZ6rZgCD4uh1xzcHcNN/sKmDc" />
</div>
    <div>
        
        
        <h1>
            YuvaVaradhi</h1>
        <fieldset class="row1">
            <p>
                <label>
                    PKey No. *
                </label>
                
                
                <input name="txtPKey" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;txtPKey\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="txtPKey" />
                <span id="RegularExpressionValidator1" style="visibility:hidden;">Only Numbers allowed</span>
            </p>
        </fieldset>
        <fieldset class="row2">
            <legend>CAN Details </legend>
            <p>
                <label>
                    CAN
                </label>
                <input name="txtCAN" type="text" id="txtCAN" class="long" readonly="" />
            </p>
            <p>
                <label>
                    Division
                </label>
                <input name="txtDivision" type="text" id="txtDivision" class="long" readonly="" />
            </p>
            <p>
                <label>
                    Subdivision
                </label>
                <input name="txtSubDivision" type="text" id="txtSubDivision" class="long" readonly="" />
            </p>
            <p>
                <label>
                    Section
                </label>
                <input name="txtSection" type="text" id="txtSection" class="long" readonly="" />
            </p>
            <p>
                <label>
                    Consumer Name
                </label>
                <input name="txtConsumerName" type="text" id="txtConsumerName" class="long" readonly="readonly" />
            </p>
            <p>
                <label>
                    Address
                </label>
                
                
                <textarea name="txtadd" rows="2" cols="20" readonly="readonly" id="txtadd" style="color:#505050;border-color:#E1E1E1;border-width:1px;border-style:solid;width:240px;">
</textarea>
                
            </p>
            <p>
                <label>
                    Email ID
                </label>
                <input name="txtEmailID" type="text" id="txtEmailID" class="long" readonly="" />
            </p>
            
            
            <p>
                <div class="table-responsive">
                    
                </div>
            </p>
        </fieldset>
        <fieldset class="row3">
            <legend>Complaint Details </legend>
            <p>
                <label>
                    Complaint No
                </label>
                <input name="txtComplaintNo" type="text" id="txtComplaintNo" class="long" readonly="" />
            </p>
            <p>
                <label>
                    Complaint Source
                </label>
                <input name="txtComplaintSource" type="text" id="txtComplaintSource" class="long" readonly="" />
            </p>
            <p>
                <label>
                    Grievance Type
                </label>
                <input name="txtGrievanceType" type="text" id="txtGrievanceType" class="long" readonly="" />
            </p>
            <p>
                <label>
                    Grievance Reason
                </label>
                
                <textarea name="txtGrievanceReason" rows="2" cols="20" readonly="readonly" id="txtGrievanceReason" style="color:#505050;border-color:#E1E1E1;border-width:1px;border-style:solid;width:240px;">
</textarea>
            </p>
            <p>
                <label>
                    Grievance Status
                </label>
                <input name="txtGrievanceStatus" type="text" id="txtGrievanceStatus" class="long" readonly="" />
            </p>
            <p>
                <label>
                    Is Reopened
                </label>
                <input name="txtIsReopened" type="text" id="txtIsReopened" class="long" readonly="" />
            </p>
            <p>
                <label>
                    Complaint Desc
                </label>
                
                <textarea name="txtComplaintDesc" rows="2" cols="20" readonly="readonly" id="txtComplaintDesc" style="color:#505050;border-color:#E1E1E1;border-width:1px;border-style:solid;width:240px;">
</textarea>
            </p>
            <p>
                <label>
                    Recieved Date
                </label>
                <input name="txtRecievedDate" type="text" id="txtRecievedDate" class="long" readonly="" />
            </p>
            <p>
                <label>
                    Rectified Date
                </label>
                <input name="txtRectifiedDate" type="text" id="txtRectifiedDate" class="long" readonly="" />
            </p>
            <p>
                <label>
                    Cotact No
                </label>
                <input name="txtContact" type="text" id="txtContact" class="long" readonly="" />
            </p>
            <p>
                <label>
                    CRA Comments
                </label>
                <input name="txtCRAComments" type="text" id="txtCRAComments" class="long" readonly="" />
            </p>
            <p>
                <label>
                    No.of Days taken to redress the complaint
                </label>
                <input name="txtRedress" type="text" id="txtRedress" class="long" readonly="" />
            </p>
            
        </fieldset>
        <fieldset class="row4">
            <p class="agreement">
                <label>
                    1) Survey Mode? *
                </label>
                <select name="ddlSurvey" id="ddlSurvey">
	<option value="0">Select</option>
	<option value="1">Field Visit</option>
	<option value="2">Phone Call</option>

</select>
            </p>
            <p class="agreement">
                <label>
                    2) Is your Complaint resolved? *
                </label>
                <select name="ddlComplaint" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ddlComplaint\&#39;,\&#39;\&#39;)&#39;, 0)" id="ddlComplaint">
	<option selected="selected" value="0">Select</option>
	<option value="1">Yes</option>
	<option value="2">No</option>

</select>
                
            </p>
            <p class="agreement">
                <label>
                    3) Are you satisfied with our services? *
                </label>
                <select name="ddlServcies" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ddlServcies\&#39;,\&#39;\&#39;)&#39;, 0)" id="ddlServcies">
	<option selected="selected" value="0">Select</option>
	<option value="1">Yes</option>
	<option value="2">No</option>

</select>
                
            </p>
            <p class="agreement">
                <label>
                    4) How was the staff behaviour? *
                </label>
                <select name="ddlStaff" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ddlStaff\&#39;,\&#39;\&#39;)&#39;, 0)" id="ddlStaff">
	<option selected="selected" value="0">Select</option>
	<option value="1">Good</option>
	<option value="2">Not so Good</option>

</select>
                
            </p>
            <p class="agreement">
                <label id="lblDrinkingWater">
                    5) Which water using for drinking Purpose? *
                </label>
                <select name="ddlDrinkingWater" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ddlDrinkingWater\&#39;,\&#39;\&#39;)&#39;, 0)" id="ddlDrinkingWater">
	<option selected="selected" value="0">Select</option>
	<option value="1">HMWSSB Water</option>
	<option value="2">HMWSSB Water with RO</option>
	<option value="3">BoreWell Water with RO</option>
	<option value="4">Mineral Water</option>
	<option value="5">Others</option>

</select>
                
            </p>
            
            </fieldset>
             <h5>Upload File</h5>
        <label for="fileSelect">Filename:</label>
        <input type="file" name="photo" id="fileSelect"
        <input type="submit" name="submit" value="Upload">
        <p><strong>Note:</strong> Only .jpg, .jpeg, .gif, .png formats allowed to a max size of 5 MB.</p>
            
            
            
            
        <div>
            <button onclick="if (typeof(Page_ClientValidate) == 'function') Page_ClientValidate(''); __doPostBack('btnSubmit','')" id="btnSubmit" class="button">
                Submit &raquo;</button>
            
        </div>
        
    </div>
    
<script type="text/javascript">
//<![CDATA[
var Page_Validators =  new Array(document.getElementById("RegularExpressionValidator1"));
//]]>
</script>

<script type="text/javascript">
//<![CDATA[
var RegularExpressionValidator1 = document.all ? document.all["RegularExpressionValidator1"] : document.getElementById("RegularExpressionValidator1");
RegularExpressionValidator1.controltovalidate = "txtPKey";
RegularExpressionValidator1.errormessage = "Only Numbers allowed";
RegularExpressionValidator1.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";
RegularExpressionValidator1.validationexpression = "\\d+";
//]]>
</script>


<script type="text/javascript">
//<![CDATA[

var Page_ValidationActive = false;
if (typeof(ValidatorOnLoad) == "function") {
    ValidatorOnLoad();
}

function ValidatorOnSubmit() {
    if (Page_ValidationActive) {
        return ValidatorCommonOnSubmit();
    }
    else {
        return true;
    }
}
        //]]>
</script>
</form>
</body>
</html>
